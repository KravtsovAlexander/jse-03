package ru.t1.kravtsov.tm;

import static ru.t1.kravtsov.tm.constant.TerminalConst.*;

public class Application {

    public static void main(String[] args) {
        displayWelcome();
        parseArguments(args);
    }

    private static void parseArguments(final String[] args) {
        if (args == null || args.length == 0) return;

        final String arg = args[0];
        switch (arg) {
            case VERSION:
                displayVersion();
                break;
            case ABOUT:
                displayAbout();
                break;
            case HELP:
                displayHelp();
                break;
        }

        System.exit(0);
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void displayHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - Display program version.\n", VERSION);
        System.out.printf("%s - Display developer info.\n", ABOUT);
        System.out.printf("%s  - Display list of terminal commands.\n", HELP);
    }

    public static void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.3.0");
    }

    public static void displayAbout() {
        System.out.println("[DEVELOPER]");
        System.out.println("NAME: Alexander Kravtsov");
        System.out.println("EMAIL: aekravtsov@nota.tech");
    }

}