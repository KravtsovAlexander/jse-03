package ru.t1.kravtsov.tm.constant;

public class TerminalConst {

    public static final String VERSION = "version";

    public static final String HELP = "help";

    public static final String ABOUT = "about";

}
